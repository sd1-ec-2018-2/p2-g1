const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static('public'));

let proxies = {};

app.get('/', function (req, res) {
	let file;

	if (req.cookies.id)
		file = 'index.html';
	else
		file = 'login.html';

	res.sendFile(path.join(__dirname, file));
});

server.listen(process.env.PORT || 3000, function () {
	console.log('Server listening on port ' + (process.env.PORT || 3000) + '...');
});


io.use((socket, next) => {
	let id = socket.handshake.query.id;
	if (proxies.hasOwnProperty(id))
		next();
	else
		next(new Error('invalid id'));
});

io.on('connection', function (socket) {
	let id = socket.handshake.query.id;
	let proxy = proxies[id];
	proxy.socket = socket;

	socket.on('error', function (error) {
		console.log('[SOCKET ERROR] ', error);
	});

	proxy.close = function (message) {
		console.log('Deleting client ' + id + ' with message: ' + message);
		proxy.client.disconnect(message);
		delete proxies[id];
	}

	socket.on('disconnect', function () {

		if (!proxy.quit)
			proxy.timeout = setTimeout(proxy.close, 30000);
	});

	clearTimeout(proxy.timeout);

	if (!proxy.client) {
		proxy.client = new irc.Client(proxy.servidor, proxy.nick, {
			userName: proxy.nick,
			realName: proxy.nick,
			channels: proxy.canais
		});
		proxy.canais = [];
		proxy.registered = false;

		setEventsOn(proxy);
		setChannelsEvents(proxy);
	}


	setComandsOn(proxy);

	socket.emit('info inicial', {
		nick: proxy.nick,
		servidor: proxy.servidor,
		canais: proxy.canais,
		prefixCanal: proxy.client.opt.channelPrefixes,
		registered: proxy.registered
	});

	if (proxy.registered)
		proxy.client.list();
});

function setComandsOn(proxy) {
	let client = proxy.client;
	client.addListener('error', function (message) {
		proxy.socket.emit('irc error', {
			text: message.args[message.args.length - 1]
		});
	});

	client.addListener('registered', function () {
		proxy.registered = true;
		proxy.socket.emit('registered');
		client.list();
	});

	client.addListener('nick', function (oldnick, newnick, channels) {
		proxy.socket.emit('nick', {
			oldnick: oldnick,
			newnick: newnick,
			channels: channels
		});
	});

	client.addListener('pm', function (nick, text) {
		proxy.socket.emit('pm', {
			from: nick,
			text: text
		});
	});

	client.addListener('notice', function (nick, to, text) {
		if (!nick)
			nick = 'Server';

		proxy.socket.emit('notice', {
			from: nick,
			to: to,
			text: text
		});
	});

	client.addListener('channellist_start', function () {
		proxy.socket.emit('channellist_start');
	});

	client.addListener('channellist_item', function (channel) {
		proxy.socket.emit('channellist_item', channel);
	});

	client.addListener('motd', function (motd) {
		proxy.socket.emit('motd', motd);
	});

	client.addListener('whois', function (info) {
		proxy.socket.emit('whois', info);
	});

	client.addListener('+mode', function (channel, by, mode, argument) {
		proxy.socket.emit('mode', {
			channel: channel,
			by: by,
			mode: '+' + mode,
			argument: argument
		});
	});

	client.addListener('-mode', function (channel, by, mode, argument) {
		proxy.socket.emit('mode', {
			channel: channel,
			by: by,
			mode: '-' + mode,
			argument: argument
		});
	});

	client.addListener('quit', function (nick, reason, channels) {
		proxy.socket.emit('quit', {
			nick: nick,
			reason: reason,
			channels: channels
		});
	});

	client.addListener('kill', function (nick, reason, channels) {
		proxy.socket.emit('kill', {
			nick: nick,
			reason: reason,
			channels: channels,
		});
	});
}

function setEventsOn(proxy) {

	let client = proxy.client;

	client.addListener('error', function (message) {
		proxy.socket.emit('irc error', {
			text: message.args[message.args.length - 1]
		});
	});

	client.addListener('registered', function () {
		proxy.registered = true;
		proxy.socket.emit('registered');
		client.list();
	});

	client.addListener('nick', function (oldnick, newnick, channels) {
		proxy.socket.emit('nick', {
			oldnick: oldnick,
			newnick: newnick,
			channels: channels
		});
	});

	client.addListener('pm', function (nick, text) {
		proxy.socket.emit('pm', {
			from: nick,
			text: text
		});
	});

	client.addListener('notice', function (nick, to, text) {
		if (!nick)
			nick = 'Server';

		proxy.socket.emit('notice', {
			from: nick,
			to: to,
			text: text
		});
	});

	client.addListener('channellist_start', function () {
		proxy.socket.emit('channellist_start');
	});

	client.addListener('channellist_item', function (channel) {
		proxy.socket.emit('channellist_item', channel);
	});

	client.addListener('motd', function (motd) {
		proxy.socket.emit('motd', motd);
	});

	client.addListener('whois', function (info) {
		proxy.socket.emit('whois', info);
	});

	client.addListener('+mode', function (channel, by, mode, argument) {
		proxy.socket.emit('mode', {
			channel: channel,
			by: by,
			mode: '+' + mode,
			argument: argument
		});
	});

	client.addListener('-mode', function (channel, by, mode, argument) {
		proxy.socket.emit('mode', {
			channel: channel,
			by: by,
			mode: '-' + mode,
			argument: argument
		});
	});

	client.addListener('quit', function (nick, reason, channels) {
		proxy.socket.emit('quit', {
			nick: nick,
			reason: reason,
			channels: channels
		});
	});

	client.addListener('kill', function (nick, reason, channels) {
		proxy.socket.emit('kill', {
			nick: nick,
			reason: reason,
			channels: channels,
		});
	});
}

function setChannelsEvents(proxy) {
	let client = proxy.client;

	client.addListener('message#', function (nick, to, text) {
		proxy.socket.emit('message', {
			from: nick,
			to: to,
			text: text
		});
	});

	client.addListener('join', function (channel, nick) {
		if (nick === proxy.nick)
			proxy.canais.push(channel);

		proxy.socket.emit('join', {
			channel: channel,
			nick: nick
		});
	});

	client.addListener('part', function (channel, nick, reason) {
		if (nick === proxy.nick)
			proxy.canais.splice(proxy.canais.indexOf(channel), 1);

		proxy.socket.emit('part', {
			channel: channel,
			nick: nick,
			reason: reason
		});
	});

	client.addListener('names', function (channel, nicks) {
		proxy.socket.emit('names', {
			channel: channel,
			nicks: Object.keys(nicks),
			modifiers: nicks
		});
	});

	client.addListener('quit', function (nick, reason, channels) {
		proxy.socket.emit('quit', {
			nick: nick,
			reason: reason,
			channels: channels
		});
	});

	client.addListener('invite', function (channel, from) {
		proxy.socket.emit('invite', {
			channel: channel,
			from: from
		});
	});

	client.addListener('kick', function (channel, nick, by, reason) {
		proxy.socket.emit('kick', {
			channel: channel,
			nick: nick,
			reason: reason,
			by: by,
		});
	});

	client.addListener('topic', function (channel, topic, nick) {
		proxy.socket.emit('topic', {
			channel: channel,
			topic: topic,
			nick: nick
		});
	});

	client.addListener('action', function (from, to, text) {
		proxy.socket.emit('action', {
			from: from,
			to: to,
			text: text
		});
	});
}


function obtenhaIDNovoLogin(proxies) {
	const crypto = require("crypto");
	let id;
	do {
		id = crypto.randomBytes(16).toString("hex");
	} while (proxies[id]);
	return id;
}

app.post('/login', function (req, res) {

	if (!(req.body.servidor && req.body.nome && req.body.canal)) {
		return res.redirect('/');
	}

	let id = obtenhaIDNovoLogin(proxies);

	proxies[id] = {
		nick: req.body.nome,
		servidor: req.body.servidor,
		canais: [req.body.canal],
		socket: null,
		client: null
	};

	res.cookie('id', id);
	res.redirect('/');
});
