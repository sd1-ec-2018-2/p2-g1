
let comandos = {};

comandos.action = function (socket, args) {
	if (args.length >= 3) {
		let data = {
			target: args[1],
			message: args.slice(2).join(' ')
		};

		socket.emit('action', data);
	}
};


comandos.msg = function (socket, args) {
	let data = {
		to: args[1],
		message: args.slice(2).join(' ')
	};

	socket.emit('message', data);
	let escopo = (ehCanal(data.to) ? data.to : 'inicio');
	let alvo = (escopo == 'inicio' ? data.to : null);
	writeMessageOn(data.message, escopo, globais.nick, alvo);
};

comandos.notice = function (socket, args) {
	if (args.length >= 2) {
		let data = {
			to: args[1],
			message: args.slice(2).join(' ')
		};

		socket.emit('notice', data);
	}

};

comandos.nick = function (socket, args) {
	socket.emit('nick', args[1]);
};

comandos.names = function (socket, args) {
	if (args.length >= 2) {
		socket.emit('names', {
			channel: args[1]
		});
	}
};



comandos.join = function (socket, args) {
	let canais = args.slice(1).join(',');
	socket.emit('join', canais);
};

comandos.motd = function (socket, args) {
	socket.emit('motd');
};

comandos.whois = function (socket, args) {
	if (args.length == 2)
		socket.emit('whois', args[1]);
};

comandos.list = function (socket, args) {
	socket.emit('list');
};

comandos.mode = function (socket, args) {
	if (args.length >= 3)
		socket.emit('mode', args.slice(1));
};

comandos.part = function (socket, args) {
	if (args.length >= 2) {
		let data = {
			canal: args[1],
			mensagem: args.slice(2).join(' ')
		};

		socket.emit('part', data);
	}
};

comandos.quit = function (socket, args) {
	socket.emit('quit', args.slice(1).join(' '));
	Cookies.remove('id');
	window.location = "/";
};

function userEvents(socket) {

	$('#mensagem-form').submit(function () {
		let mensagem = $('#mensagem').val().trim();
		$('#mensagem').val('');

		if (!mensagem)
			return false;

		if (mensagem.charAt(0) !== '/') {

			let canal = $('.canais-nav .active').attr('canal');
			if (!canal) {
				setError('Na aba inicial é necessário especificar o alvo através do comando /msg', $('.canais-nav .active').attr('canal') || 'inicio');
				return false;
			}

			let comando = '/msg ' + canal + ' ' + mensagem;
			comandos.msg(socket, comando.split(' '));
		} else {
			let args = mensagem.split(' ');
			let comando = args[0].substr(1);

			if (comandos.hasOwnProperty(comando))
				comandos[comando](socket, args);
			else
				setError(args[0] + ': Comando desconhecido!', $('.canais-nav .active').attr('canal') || 'inicio');
		}

		return false;
	});

	$('.canais-nav li').click(mudarAba);
	$('#atualizar-canais').click(function () {
		comandos.list(socket, ['/list']);
	});
	$('#atualizar-names').click(function () {
		let canal = $('.canais-nav .active').attr('canal');
		let comando = '/names ' + canal;
		comandos.names(socket, comando.split(' '));
		$('.name-ch-' + canal.replace('#', '')).remove();
	});


	$('.mode-menu li').click(function () {
		return false;
	});
	$('.mode-menu span').click(function (evt) {
		let $target = $(this),
			mode = $target.closest('a').data('mode'),
			contexto = $('.canais-nav .active').attr('canal') || 'inicio',
			comando = '/mode ';

		if (mode.substr(0, 3) === 'ch-') {
			if (contexto === 'inicio')
				return setError('O modo selecionado é apenas para canais', contexto);
			else
				comando += contexto;
		} else {
			comando += globais.nick;
		}

		if ($target.hasClass('glyphicon-plus'))
			comando += ' +' + mode.replace('ch-', '');
		else
			comando += ' -' + mode.replace('ch-', '');

		comandos.mode(socket, comando.split(' '));
		return false;
	});

	$('#sair').click(function () {
		comandos.quit(socket, ['/quit']);
	});
}
