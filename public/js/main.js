(function ($) {
	"use strict";


	/*==================================================================
	[ Validate ]*/
	let input = $('.validate-input .input100');

	$('.validate-form').on('submit', function () {
		let check = true;

		for (let i = 0; i < input.length; i++) {
			if (validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
			}
		}

		return check;
	});


	$('.validate-form .input100').each(function () {
		$(this).focus(function () {
			hideValidate(this);
		});
	});

	function validate(input) {
		if ($(input).attr('type') == 'username' || $(input).attr('name') == 'username') {
			if ($(input).val().trim().match(/[^\w\.\_]+/)) {
				return false;
			}
		}
		else {
			if ($(input).val().trim() == '') {
				return false;
			}
		}
	}

	function showValidate(input) {
		let thisAlert = $(input).parent();

		$(thisAlert).addClass('alert-validate');
	}

	function hideValidate(input) {
		let thisAlert = $(input).parent();

		$(thisAlert).removeClass('alert-validate');
	}


})(jQuery);

function setServidor(server, canal) {
	$('#servidor').val(server);

	if (!$('#canal').val())
		$('#canal').val(canal);
}