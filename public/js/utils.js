

function anunciar(mensagem, escopo) {
	escopo = escopo.replace('#', 'ch-');
	writeText('<p class="anuncio text-center msg-' + escopo + '">' + mensagem + '</p>');
}


function writeMessageOn(mensagem, escopo, autor, alvo) {
	escopo = escopo.replace('#', 'ch-');
	let html = '<div class="row mensagem msg-' + escopo + '"><div class="col-xs-12 col-sm-3 col-md-2"><p>';

	let autorCru = autor.replace('[', '').replace(']', ''); 
	if (autorCru == 'Server' || autorCru == globais.nick)
		html += '<span class="text-info">' + autor + '</span>';
	else
		html += '<span class="autor">' + autor + '</span>';

	if (alvo) {
		html += ' => ';
		if (alvo == 'Server' || alvo == globais.nick)
			html += '<span class="text-info">' + alvo + '</span>';
		else
			html += '<span class="autor">' + alvo + '</span>';
	}

	html += '</p></div><div class="col-xs-12 col-sm-9 col-md-10">'
		+ '<p>' + mensagem + '</p>'
		+ '</div></div>';

	writeText(html);
}

function setError(mensagem, escopo) {
	escopo = escopo.replace('#', 'ch-');
	writeText('<p class="erro bg-danger text-danger text-center msg-' + escopo + '">' + mensagem + '</p>');
}


function writeText(texto) {
	let mural = $("#mural");
	mural.append(texto);
	sincronizarContexto();
	mural.scrollTop(mural[0].scrollHeight);
}


function sincronizarContexto() {
	let canal = $('.canais-nav .active').attr('canal');
	$('#mural').children().not('.msg-global').addClass('hidden');
	$('.painel-canais, .painel-names').addClass('hidden');
	$('.painel-names p[nome]').addClass('hidden');

	if (!canal) {
		$('.msg-inicio').removeClass('hidden');
		$('#titulo-painel').text('Canais');
		$('.painel-canais').removeClass('hidden');
		$('.mode-menu .ch-mode').addClass('hidden');
	} else {
		canal = canal.replace('#', '');
		$('.msg-ch-' + canal).removeClass('hidden');
		$('#titulo-painel').text('Nomes');
		$('.painel-names').removeClass('hidden');
		$('.mode-menu .ch-mode').removeClass('hidden');
		$('.name-ch-' + canal).removeClass('hidden');
	}
}

function adicinarAba(canal, irParaTab) {
	let tab = $('<li canal="' + canal + '"><a>' + canal + '</a></li>');
	tab.click(mudarAba);
	$('.canais-nav').append(tab);
	if (irParaTab)
		mudarAba(null, tab);
}


function removerAba(canal) {
	$('.msg-ch-' + canal.replace('#', '')).remove();
	$('.name-ch-' + canal.replace('#', '')).remove();
	let tab = $('.canais-nav li[canal="' + canal + '"]');

	if (tab.hasClass('active'))
		mudarAba(null, $('.canais-nav li').first());

	tab.remove();
}


function mudarAba(evt, tab) {
	if (!tab)
		tab = $(this);

	if (tab.hasClass('active'))
		return;

	$('.canais-nav li').removeClass('active');
	tab.addClass('active');
	sincronizarContexto();
	$('#mural').scrollTop($('#mural')[0].scrollHeight);
}

function adicionarCanal(canal, numUsuarios) {
	let item = $('<p>' + canal + ' (' + numUsuarios + ')</p>');
	item.click(onClickChannelItem);
	$('.painel-canais').append(item);
}

function onClickChannelItem() {
	let canal = $(this).text().split(' ')[0];
	$('#mensagem').val('/join ' + canal);
	$('#mensagem-form').submit();
}

function onClickNameItem() {
	$('#mensagem').val('/msg ' + $(this).attr('nome') + ' ');
	$('#mensagem').focus();
}

function adicionarNome(canal, nome, modificador, atualizar) {
	canal = canal.replace('#', '');
	let item = $('<p nome="' + nome + '">' + modificador + nome + '</p>');
	item.addClass('name-ch-' + canal);

	if (nome === globais.nick)
		item.css({
			'text-decoration': 'underline',
			'cursor': 'default'
		});
	else
		item.click(onClickNameItem);

	$('.painel-names').append(item);
	if (atualizar)
		sincronizarContexto();
}


function ehCanal(str) {
	let c = globais.prefixCanal.split('');
	for (let i = 0; i < c.length; i++) {
		if (str.charAt(0) === c[i])
			return true;
	}
	return false;
}

$(function () {
	let id = Cookies.get("id");
	let socket = io('/?id=' + id);

	socketHandler(socket);
	userEvents(socket);

	globais.socket = socket;
	$('#mensagem').focus();
});

let globais = {
	nick: null,
	servidor: null,
	canais: [],
	prefixCanal: '#',
	registered: false
};
